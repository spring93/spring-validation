package com.example.spring.validation.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponse {
	@JsonInclude(Include.NON_NULL)
	private Integer status;
	
	@JsonInclude(Include.NON_NULL)
	private String message;
	
	@JsonInclude(Include.NON_NULL)
	private LocalDateTime timestamp;
	
	@JsonInclude(Include.NON_NULL)
	private Object errors;
	
	public ErrorResponse(Integer status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public ErrorResponse(Integer status, String message, LocalDateTime timestamp) {
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
	}
}
