package com.example.spring.validation.exception;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.spring.validation.response.ErrorResponse;

@ControllerAdvice
public class CustomGlobalException extends ResponseEntityExceptionHandler {
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorResponse body = ErrorResponse.builder()
				.status(status.value())
				.message(status.name())
				.timestamp(LocalDateTime.now())
				.errors(ex.getBindingResult()
						.getFieldErrors()
						.stream()
						.map(x -> x.getDefaultMessage())
						.collect(Collectors.toList()))
				.build();
        return new ResponseEntity<>(body, headers, status);
    }
}
