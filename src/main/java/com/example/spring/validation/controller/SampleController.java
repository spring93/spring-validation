package com.example.spring.validation.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.spring.validation.request.OtherRequest;
import com.example.spring.validation.request.SampleRequest;
import com.example.spring.validation.request.validator.SampleValidator;
import com.example.spring.validation.response.ErrorResponse;

@RestController
public class SampleController {
	
	@Autowired
	private SampleValidator sampleValidator;
	
	@PostMapping("test")
	public ResponseEntity<?> test(@Valid @RequestBody SampleRequest requestBody) {
		return ResponseEntity.ok("OK");
	}
	
	
	@PostMapping("test2")
	public ResponseEntity<?> test2(@RequestBody OtherRequest requestBody, BindingResult bindingResult) {
		sampleValidator.validate(requestBody, bindingResult);
		if (bindingResult.hasErrors()) {
			return new ResponseEntity(new ErrorResponse(
					HttpStatus.BAD_REQUEST.value(), 
					HttpStatus.BAD_REQUEST.name(), LocalDateTime.now(), collectError(bindingResult)), HttpStatus.BAD_REQUEST);
		}
		
		return ResponseEntity.ok("OK");
	}
	
	private static List<String> collectError(BindingResult bindingResult) {
		return bindingResult.getFieldErrors()
				.stream()
				.map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
	}
}
