package com.example.spring.validation.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SampleRequest {
	
	@NotEmpty(message = "id is required")
	private String id;
	
	@NotEmpty(message = "name is required")
	private String name;
	
	@NotNull(message = "age is required")
	private Long age;
}
