package com.example.spring.validation.request.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.example.spring.validation.request.OtherRequest;

@Component
public class SampleValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return OtherRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "error.id", "Please provide id");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.name", "Please provide name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "age", "error.age", "Please provide age");
		
		OtherRequest sr = (OtherRequest) target;
		
		if (!errors.hasErrors()) {
			if (sr.getName().equalsIgnoreCase("test")) {
				if (sr.getAge() < 10) {
					errors.rejectValue("age", "error.age", "age must greater than 20");
				}
			}
		}
	}

}
