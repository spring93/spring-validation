package com.example.spring.validation.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtherRequest {
	
	private String id;
	private String name;
	private Long age;
}
