Rest API Validation :

Tujuan :
- memvalidasi request body menggunakan bean validation (JSR 303) atau spring validator 
- handle response yg di kirimkan jika request body tidak lolos validasi.

Tools :
- spring boot 2.3.2
- lombok
- spring boot validation
- maven
- eclipse IDE


1. Buat controller advice untuk handle request yg tidak lolos validasi. 

@ControllerAdvice
public class CustomGlobalException extends ResponseEntityExceptionHandler {
....
....

2. override method handleMethodArgumentNotValid

@Override
protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,HttpHeaders headers, HttpStatus status, WebRequest request) {
...
...

bentuk struktur response yg akan di kembalikan silahkan sesuaikan dengan kebutuhan. (jika ingin di custom)

3. Buat sample request. dan silahkan tambahkan annotation validation yg ingin di gunakan. available annotation yg bisa di gunakan : javax.validation.constraints.*

public class SampleRequest {
	
	@NotEmpty(message = "id is required")
	private String id;
	
	@NotEmpty(message = "name is required")
	private String name;
...
...

4. buat sample controller 

@RestController
public class SampleController {
...
...


public ResponseEntity<?> test(@Valid @RequestBody SampleRequest requestBody) {
...
...

tambahkan annotation @Valid untuk menjalankan proses validasi. Jika validasi tidak lolos, maka akan throw MethodArgumentNotValidException
yang akan di handle oleh controler advice.

5. jalankan applikasi
mvn spring-boot:run

6. Test sample controller

Test bad request :
curl --location --request POST 'http://127.0.0.1:8080/test' \
--header 'Content-Type: application/json' \
--data-raw '{
    "age": ""
}'

sample response (bad request) :
{
    "status": 400,
    "message": "BAD_REQUEST",
    "timestamp": "2020-10-04T16:23:07.2983144",
    "errors": [
        "name is required",
        "age is required",
        "id is required"
    ]
}

Test OK :
curl --location --request POST 'http://127.0.0.1:8080/test' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": "111",
    "name": "tri",
    "age": 10
}'

sample response (OK) :
OK

7. untuk validation yg lebih flexible (suatu field required jika karna suatu kondisi lain) bisa menggunakan spring validator.
@Component
public class SampleValidator implements Validator {
...
...
if (!errors.hasErrors()) {
	if (sr.getName().equalsIgnoreCase("test")) {
		if (sr.getAge() < 10) {
			errors.rejectValue("age", "error.age", "age must greater than 20");
		}
	}
}
...
...

8. testing bad request 

curl --location --request POST 'http://127.0.0.1:8080/test2' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": "111",
    "name": "test",
    "age": 8
}'

response :
{
    "status": 400,
    "message": "BAD_REQUEST",
    "timestamp": "2020-10-04T22:01:15.7043798",
    "errors": [
        "age must greater than 20"
    ]
}

Sumber :
- https://mkyong.com/spring-boot/spring-rest-validation-example/
- https://dimitr.im/validating-the-input-of-your-rest-api-with-spring